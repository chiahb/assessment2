/**
 * Created by Francis Chia on 7/26/2016.
 */

var express = require("express");
var bodyParser = require("body-parser");
var routes = require("./routes");
var validator = require('express-validator');

var app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(validator());

app.get("/api/booklist/list", routes.listbook);
app.get("/api/books/:id", routes.getBook);
app.post("/api/books/update/", routes.updatebook);

app.use(express.static(__dirname + "/public"));

app.listen(3000, function(){
    console.info("App Server started on port 3000");
});


