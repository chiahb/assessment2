/**
 * Created by Francis Chia on 7/26/2016.
 */

(function () {

    angular
        .module("bookMgtApp", ["ui.router"])
        .controller("bookMgtCtrl", bookMgtCtrl)
        .controller("bookEditCtrl", bookEditCtrl)
        .config(function ($stateProvider, $urlRouterProvider) {

            $stateProvider
                .state("home", {
                    url: "/",
                    templateUrl: "/list.html",
                    controller: "bookMgtCtrl as ctrl"
                }).state("edit", {
                url: "/edit/:id",
                templateUrl: "/edit.html",
                controller: "bookEditCtrl as ctrl"

            });

            $urlRouterProvider.otherwise("/");


        })

    bookMgtCtrl.$inject = ["$http", "dbService"];

    function bookMgtCtrl($http, dbService) {

        var vm = this;
        vm.booklist = []; // placeholder for the results

        vm.searchBook = function () {
            dbService.getBookList(vm.book)
                .then(function (results) {
                    console.log(results);
                    vm.booklist = results.data;
                    console.log(vm.booklist);
                })
                .catch(function () {
                });
        }

        vm.searchBook();
        vm.book = {}; //placeholder for the search entries
        vm.status = {
            message: "",
            code: 0
        };


        var vm = this;
        vm.booklist = []; // placeholder for the results
        /*vm.edit = function (bookId) {
         vm.bookinfo.id = "";
         vm.bookinfo.title = "";
         vm.bookinfo.author_lastname = "";
         vm.bookinfo.author_firstname = "";
         vm.status = {
         message: "",
         code: 0
         };
         vm.bookinfo.id = bookId;
         $http.post("/api/bookinfo/update/", vm.bookinfo)
         .then(function () {
         vm.status.message = "The book is updated to the database.";
         vm.status.code = 202;
         }).catch(function () {
         console.info("Error");
         vm.status.message = "Failed to update book to the database.";
         vm.status.code = 400;
         });*/


    }


    bookEditCtrl.$inject = ["$http", "dbService","$stateParams"];

    function bookEditCtrl($http, dbService,$stateParams) {

        var vm = this;
        var url = "/api/books/" + $stateParams.id
        $http.get(url)
            .then(function (book) {
                vm.bookinfo = book.data[0];
            });
        
        vm.edit = function () {
            $http.post("/api/books/update/", vm.bookinfo)
                .then(function () {
                    vm.status.message = "The book is updated to the database.";
                    vm.status.code = 202;
                }).catch(function () {
                console.info("Error");
                vm.status.message = "Failed to update book to the database.";
                vm.status.code = 400;
            });
        }
    }
    function createBook() {
        return ({title: '', author: ''});
    }

})();
