/**
 * Created by Francis Chia on 7/26/2016.
 */

(function(){
    angular
        .module("bookMgtApp")
        .service ("dbService", dbService);

    dbService.$inject = ["$http", "$q"];

    function dbService ($http, $q) {

        var service = this;

        service.getBook = function () {
            var booklist = {};
            booklist.title = "";
            booklist.author = "";
            return booklist;
        };

        service.getBookList = function(book) {
            var defer = $q.defer();
            $http.get("/api/booklist/list", {
                params: book
            }).then(function (results) {
                defer.resolve(results);
            }).catch(function (err) {
                defer.reject(err);
            });

            return defer.promise;
        }
        
        
        
        
    }
    
    
})();