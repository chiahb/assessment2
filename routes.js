/**
 * Created by Francis Chia on 7/26/2016.
 */

var BookSQL = require("./tasksSQL");

exports.getBook = function (req, res) {
    
    var params = req.params.id;
    BookSQL.getBook(req.params, function (err, results) {
        console.log(results);
        if (err) {
            console.error(err);
            return res.status(500).end();
        }
        res.status(202).json(results);
    })
    
}

exports.listbook = function (req, res) {

    var params = req.params.book;
    console.log (req.query)
    BookSQL.getAllBook(req.query, function (err, results) {
        console.log(results);
        if (err) {
            console.error(err);
            return res.status(500).end();
        }
        res.status(202).json(results);
    })
}


exports.updatebook = function (req, res) {
    BookSQL.updateBook(req.body, function (err, results) {
        console.log(results);
        if (err) {
            console.error(err);
            return res.status(500).end();
        }
        res.status(202).json(results);
        });
}


/*
exports.listbookodo = function (req, res) {

    var title = req.body.title;
    var author = req.body.author;

    var booklist = new BookSQL(
        title,
        author

    );
};*/


/*todolist.save(function(err, result){
 if (err) {
 res.status(500).end();
 return console.error("Some Error occured", err);
 }
 console.info(result);
 res.status(202).json({id: result.insertId});
 });
 };*/
