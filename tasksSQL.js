/**
 * Created by Francis Chia on 7/26/2016.
 */


var mysql = require("mysql");

var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "root",
    password: "password",
    database: "books_lib",
    connectionLimit: 4
});

//const INSERTSQL = "insert into books (title, author) values (?,?)";
const SELECTSQL = "select * from books where title =? OR author_lastname = ? OR author_firstname = ? limit 10";
const SELECTIDSQL = "select * from books where id = ?";
const UPDATESQL = "update books SET title=?, author_lastname=?, author_firstname=? WHERE id=?";

var booklist = function (title, author_lastname, id) {
    this.title = title;
    this.author_lastname = author_lastname;
    this.id = id;
};

booklist.getAllBook = function (params, callback) {
    if(! params ) {
        params = {
            title: "",
            author: ""
        };
    }
    console.log(params)
    var booklist = this;
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err);
        }
        connection.query(SELECTSQL, [params.title, params.author_lastname, params.author_firstname], function (err, results) {
            connection.release();
            console.log(results)
            if (err) {
                return callback(err);
            }
            callback(null, results);
        });
    });
};


booklist.getBook = function (params, callback) {
    if(! params ) {
        params = {
            title: "",
            author: ""
        };
    }
    console.log(params)
    var booklist = this;
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err);
        }
        connection.query(SELECTIDSQL, [params.id], function (err, results) {
            connection.release();
            console.log(results)
            if (err) {
                return callback(err);
            }
            callback(null, results);
        });
    });
};

booklist.updateBook = function (params, callback) {
    if(! params ) {
        params = {
            title: "",
            author: ""
        };
    }
    console.log(params)
    var booklist = this;
    pool.getConnection(function (err, connection) {
        if (err) {
            return callback(err);
        }
        connection.query(UPDATESQL, [params.title, params.author_lastname, params.author_firstname, params.id], function (err, results) {
            connection.release();
            console.log(results)
            if (err) {
                return callback(err);
            }
            callback(null, results);
        });
    });
};

module.exports = booklist;


/*const updateOneBook = "UPDATE books SET title = ?, author_lastname = ?, author_firstname =? where id = ?";
var makeQuery = function (sql, pool) {
    return (function (args) {
        var defer = q.defer();
        pool.getConnection(function (err, conn) {
            if (err) {

                defer.reject(err);
                return;
            }
            console.log(args);
            conn.query(sql, args || [], function (err, results) {
                conn.release();
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve(results);
            });
        });
        return (defer.promise);
    });
};
var updateOne = makeQuery(updateOneBook, pool);

app.post("/api/bookinfo/update/", function (req, res) {
    console.info(req);
    updateOne([req.body.title, req.body.author_lastname, req.body.author_firstname,req.body.id])
        .then(function (results) {
            if (results) {
                res.status(200).json(results[0]);
            }
            else {
                // console.log(results.length);
                res.status(404).end();
            }
        });
});*/